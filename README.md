# :man_astronaut: Basics

## 📖 Philosophie

Unser Ziel ist es funktionale, schöne und performante Websites & Anwendungen zu entwickeln. Wir nutzen keinen Copy-Paste Code, den wir nicht verstehen und anpassen können. Jedes Projekt wird mit [Git](/git/) versioniert. Wir schreiben einheitlichen, sauberen, und leicht wartbaren Code. Um das zu erreichen ist der Styleguide bei **jedem** Projekt anzuwenden. Besonders bei großen Projekten ist dies unabdingbar, um spätere Probleme bei der Wartung/Erweiterung zu vermeiden. :+1:

## 💻 Setup

- Editor: [VS Code](https://code.visualstudio.com/)
- Terminal: [iTerm2](https://www.iterm2.com/)
- Projektmanagement: [Trello](https://trello.com) & [Slack](https://slack.com)
- Git: [Bitbucket](https://bitbucket.org)
- Git-Client (optional): [Sourcetree](https://www.sourcetreeapp.com/)
- Hosting für statische Seiten: [Netlify](https://netlify.com)
- Package Manager: [Yarn](https://yarnpkg.com/lang/en/)
- [Node.js](https://nodejs.org/en/) (für serverseitige Apps)
- [MAMP](https://www.mamp.info/de/) (für LEMP Projekte)
- FTP-Client: [FileZilla](https://filezilla-project.org/)

## 🚀 Stack

### JAMstack

Wenn das Projekt es zulässt, sollte bei der Umsetzung immer der [JAMstack](https://jamstack.org/ 'Zum JAMstack') (JavaScript, APIs, Markup) verwendet werden. Dabei wird am Ende eine statische Seite an den Nutzer ausgeliefert. Das hat folgende Vorteile:

- Bessere Performance
- Höhere Sicherheit
- Deployment ist durch Git erheblich einfacher

JAMstack Bestandteile:

- **J**avaScript: [Vue.js](https://vuejs.org)
- **A**PI: Headless WordPress
- **M**arkup: [Nuxt.js](https://nuxtjs.org/)

Dabei agiert WordPress als reines CMS, das die Inhalte über die integrierte REST-API an das Vue.js Frontend ausliefert. Dadurch ist das Frontend komplett von WordPress losgelöst und kann individuell aufgebaut werden.

Das fertige Projekt wird auf [Netlify](https://netlify.com) gehostet. Bei Änderungen am verknüpften Git-Repository oder inhaltlichen Änderungen in WordPress (WebHooks) wird der Build-Prozess automatisch ausgeführt. Dadurch ist kein kompliziertes Deployment mehr nötig.

![alt text](https://media.giphy.com/media/12NUbkX6p4xOO4/giphy.gif 'Magic')

#### Workflow

### LEMP stack

- **L**: Linux
- **E**: nginx (engine x)
- **M**: MySQL
- **P**: PHP
