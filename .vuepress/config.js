module.exports = {
  title: 'connect2 Coding Styleguide',
  description: 'Offizieller connect2 Coding Styleguide',
  themeConfig: {
    nav: [
      { text: 'Basics', link: '/' },
      { text: 'WordPress', link: '/wordpress/' },
      { text: 'Git', link: '/git/' },
      { text: 'JavaScript', link: '/js/' },
      { text: 'CSS', link: '/css/' },
      { text: 'Vue.js', link: '/vue/' }
    ],
    sidebar: 'auto',
    lastUpdated: 'Letzte Bearbeitung', // string | boolean
    serviceWorker: {
      updatePopup: {
        message: 'Neue Inhalte verfügbar',
        buttonText: 'Neu laden'
      }
    }
  }
}
